var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req, resp) {
    resp.status(200).json({
        bicicletas: Bicicleta.allBicis
    })
}

exports.bicicleta_create = function(req,resp){
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];

    Biciclteta.add(bici);

    resp.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_delete = function(req, resp){
    Bicicleta.removeById(req.body.id);
    resp.status(204).send();
}

exports.bicicleta_update = function(req, resp){
    
}