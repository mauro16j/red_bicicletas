const nodemailer = require("nodemailer");
const sgTransport = require('nodemailer-sendgrid-transport');
//
//4AeBe5KWWH88kMYNFy

/**
 * 
 * const transporter = nodemailer.createTransport({
    host: 'smtp.ethereal.email',
    port: 587,
    auth: {
        user: 'odell.blanda@ethereal.email',
        pass: '4AeBe5KWWH88kMYNFy'
    }
});
 */
let mailConfig;

if (process.env.NODE_ENV === 'production') {
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_SECRET,
        },
    };
    mailConfig = sgTransport(options);
} else {
    if (process.env.NODE_ENV === 'staging') {
        console.log('XXXXXXXXXXXXXXXX');
        const options = {
            auth: {
                api_key: process.env.SENDGRID_API_SECRET,
            },
        };
        mailConfig = sgTransport(options);
    } else {
        // all emails are catched by ethereal.email
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ethereal_user,
                pass: process.env.ethereal_pwd,
            },
        };
    }
}

module.exports = nodemailer.createTransport(mailConfig);