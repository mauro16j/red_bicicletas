var Bicicleta = require('../../models/bicicleta');
var mongoose = require('mongoose');

describe('Testing Bicicletas ', function() {
    beforeAll((done) => { mongoose.connection.close(done) });
    beforeEach(function(done){
        mongoose.disconnect();
        var mongoDB = "mongodb://localhost/testdb";
        mongoose.connect(mongoDB, { useNewUrlParser:true, useUnifiedTopology: true  });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done();
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de bicicletas', () => {
            var bici = Bicicleta.createInstance(1,"verde","urbana",[-34.5,-54.1]);
            expect(bici.code).toBe(1);
            expect(bici.color).toBe("verde");
            expect(bici.modelo).toBe("urbana");
            expect(bici.ubicacion[0]).toEqual(-34.5);
            expect(bici.ubicacion[1]).toEqual(-54.1);
        });
    });

    describe('Bicicleta.allBici', () => {
        it('Comienza vacia', function(done){
            Bicicleta.find( {}, function(err, bicis ) {
                expect(bicis.length).toBe(0);
                done();
            });
        }, 6000);
    });

    describe('Bicicleta.add', () => {
        it('Agrega una sola bicicleta', function(done){
            
            const aBici = new Bicicleta({code:1, color:"verde", modelo:"urbana"});
        
            console.log(aBici);

            Bicicleta.add(aBici, ( err, newBici) => {
                if (err) console.log(err);
                Bicicleta.find({code:1},function( err, bicis ) { 
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        }, 6000);
    });

    describe('Bicicleta.findByCode', () => {
        it('debe devolver la bici con code 1', function(done) {
            const aBici = new Bicicleta({code:1, color:"verde", modelo:"urbana"});
            
            Bicicleta.add(aBici, ( err, newBici) => {
                if (err) console.log(err);
                var aBici2 = new Bicicleta({code:2, color:"roja", modelo:"urbana"});
                Bicicleta.findByCode(1,function( err, targetBici ) { 

                    expect(targetBici.code).toEqual(aBici.code);
                    expect(targetBici.color).toEqual(aBici.color);
                    expect(targetBici.modelo).toEqual(aBici.modelo);
                    
                    done();
                    
                });
            });
        },6000);
    });
    
});

/*
beforeEach(()=> {
    Bicicleta.allBicis = [];
});

describe('Bicicleta.allBicis', ()=> {
    it('comienza vacia', ()=> {
        expect(Bicicleta.allBicis.length).toBe(0);
    });
});

describe('Bicicleta.add', ()=> {
    it('agregamos una', ()=> {
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [51.504, -0.09]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe('Bicicleta.findById', ()=> {
    it('debe devolver la bici con id 1', ()=> {
        
        expect(Bicicleta.allBicis.length).toBe(0);
        var aBici = new Bicicleta(1, 'rojo', 'urbana', [51.504, -0.09]);
        var aBici2 = new Bicicleta(2, 'blanca', 'montaña', [51.5039, -0.10]);

        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);

        var targetBici = Bicicleta.findById(1);

        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);

    });
});*/