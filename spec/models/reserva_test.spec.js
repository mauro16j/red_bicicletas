var mongoose = require('mongoose');
var Reserva = require('../../models/reserva');
var Bicicleta = require('../../models/bicicleta');
var Usuario = require('../../models/usuario');


describe('Testing Reservas ', function() {
    
    beforeEach(function(done){
        
        var mongoDB = "mongodb://localhost/testdb";
        mongoose.connect(mongoDB, { useNewUrlParser:true });

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success) {
            if (err) console.log(err);
            Usuario.deleteMany({}, function( err, success ){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success)  {
                    if (err) console.log(err);
                    done();
                });
            });
        });
    });

   
    describe('cuando se necesita actualizar una reserva', () => {
        it('debe de existir la reserva', (done) => {
            
            const usuario = new Usuario({ nombre: 'Mauricio' });
            usuario.save();
            //console.log(' usuario ' + usuario);

            const bicicleta = new Bicicleta({ code: 1, color: "verde", modelo: "urbana" });
            bicicleta.save();
            //console.log('bicicleta ' + bicicleta);

            const bicicleta2 = new Bicicleta({ code: 2, color: "Azul", modelo: "xtreme" });
            bicicleta2.save();
            //console.log('bicicleta 2 ' + bicicleta2);

            var hoy = new Date();
            var manana = new Date();
            manana.setDate(hoy.getDate() + 1);

            usuario.reservar(bicicleta._id, hoy, manana, ( err, reserva ) => {
                if( err ) console.log( err );
                Reserva.findById(reserva._id).populate('bicicleta').populate('usuario').exec(function(err, reservas) {
                    //console.log(reservas);
                    
                    //variable to update
                    var hasta = new Date();
                    hasta.setDate(hoy.getDate() + 3);

                    var reservaObj = {
                        _id: reservas._id,
                        desde: hoy,
                        hasta:  hasta,
                        bicicleta: bicicleta2._id,
                        usuario: usuario._id
                    };

                    Reserva.updateReserva( reservaObj, (err, result) =>{
                        if( err ) console.log( err );
                        //console.log(result);
                        expect(result.nModified).toBeGreaterThan(0);
                        expect(result.ok).toBe(1);
                        done();
                    });
                });
            });
        });
    });
});